import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import Home from './components/Home';
import News from './components/News';
import About from './components/About';
import Header from './components/Header';
import ContentRouter from './components/ContentRouter';

const routes = [
  { to: '/', exact: true, label: 'Home', component: Home },
  { to: '/news/', exact: false, label: 'News', component: News },
  { to: '/about/', exact: false, label: 'About', component: About },
]

function App() {
  return (
    <Router>
      <Header routes={routes} />
      <ContentRouter routes={routes} />
    </Router>
  );
}

export default App;
