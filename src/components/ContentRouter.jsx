import React from 'react';
import PropTypes from 'prop-types';
import { Route } from "react-router-dom";

const ContentRouter = props => {
    const routeObjects = props.routes.map((route) => 
        <Route path={route.to} exact={route.exact} component={route.component}/>
    );

    return (
        <>
            {routeObjects}
        </>
    );
};

ContentRouter.propTypes = {
    routes: PropTypes.arrayOf(PropTypes.object),
};

export default ContentRouter;