import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { NavLink as Link } from "react-router-dom";
import PropTypes from 'prop-types';

const Header = (props) => {
    const links = props.routes.map((route) => 
    <li className='nav-item'>
        <Link className='nav-link' to={route.to} exact={route.exact}>{route.label}</Link>
    </li>
    );

    return (
        <div>
            <Navbar bg="dark" variant="dark">
                <Navbar.Brand>Super</Navbar.Brand>
                <Nav className="mr-auto">
                    {links}
                </Nav>
            </Navbar>

        </div>
    );
};

Header.propTypes = {
    routes: PropTypes.arrayOf(PropTypes.object),
};

export default Header;
