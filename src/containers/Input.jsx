import { connect } from 'react-redux';
import { setText, incrementNumber } from '../state/actions';
import InputUnconnected from '../components/Input';

const mapStateToProps = (state) => {
    return {
        counter: state.number,
        text: state.text,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onChangeText: (text) => {
            dispatch(setText(text))
        },
        onClickButton: () => {
            dispatch(incrementNumber())
        }
    }
}

const Input = connect(
    mapStateToProps,
    mapDispatchToProps
)(InputUnconnected)

export default Input;
