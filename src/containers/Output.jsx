import { connect } from 'react-redux';
import OutputUnconnected from '../components/Output';

const mapStateToProps = (state) => {
    return {
        number: state.number,
        text: state.text,
    }
}

const Output = connect(mapStateToProps)(OutputUnconnected);

export default Output;