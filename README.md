# React Workshop

Dieses Repository befindet sich unter https://gitlab.com/benjaminbecker/react-workshop

Die einzelnen Lektionen sind über tags zugänglich. Sie können beispielweise die Lektion 6 auschecken indem Sie folgendes in Ihr Terminal tippen:

```
git checkout lesson6
```

Eine Liste der Tags bekommen Sie über
```
git tag
```

## Lektion 1: JSX and Components
### Neues React-Projekt erzeugen:
```
npx create-react-app react-workshop
```
### IDE-Empfehlung
+ Visual Studio Code
+ [React Snippets](https://marketplace.visualstudio.com/items?itemName=xabikos.ReactSnippets)

## Lektion 2: ES6, JSX, Babel
+ [Cheatsheet ES6](http://es6-features.org/#Constants)
+ [JSX](https://reactjs.org/docs/introducing-jsx.html)

## Lektion 3: Vertiefung Komponenten
Änderungen zu Lektion 1:
+ neue Komponente Header, deren Text über das Eingabefeld geändert werden soll
+ Dafür musste der state aus der Komponente HelloWithState in die Komponente App "gehoben" werden.
+ Änderung der Komponente HelloWithState zu FormWithCallback: Diese stellt ein Input-Feld zur Verfügung, welches den state der App-Komponente ändert. Dafür wird der Callback von der App-Komponente als prop übergeben.
+ Erstellung einer Komponente ComplexForm mit vereinfachter Validierung

## Lektion 4: Lifecycle und API calls
[Diagramm Lifecycle](http://projects.wojtekmaj.pl/react-lifecycle-methods-diagram/)

[Einführung state und lifecycle](https://reactjs.org/docs/state-and-lifecycle.html)

[API Reference Lifecycle-Methoden](https://reactjs.org/docs/react-component.html)

[JSON-API zum Testen](https://github.com/typicode/jsonplaceholder#how-to)

Die Komponente DisplayApiContent lädt einen Datensatz von jsonplaceholder und stellt die property "title" desselben dar.

DisplayApiContentHook tut dasselbe, wurde aber mithilfe der neuen React-Hooks-API implementiert.

## Lektion 5: React-Bootstrap und Darstellung von Arrays
```
npm install react-bootstrap bootstrap
```
CSS-Dateien von Bootstrap müssen im html-Dokument (index.html) geladen werden. Die Version kann frei gewählt werden.

Die Komponenten dieser Lektion wurden mit [react-bootstrap](https://react-bootstrap.github.io/) erstellt.

Die Komponente DisplayUsers stellt die Inhalte der Datei Users.js (json-Format) als Tabelle dar. Dabei wird die JavaScript-Funktion `map` verwendet um über die einzelnen Einträge des Arrays zu iterieren.

Die Komponente MainContent stellt den Wikipedia-Artikel über React in drei Spalten dar, die ab einer mittleren Bildschirmbreite zu drei Zeilen werden (Responsive Design).

Die Komponente ContactForm implementiert das Layout eines sehr einfachen Kontakfeldes.

## Lektion 6: FLUX, REDUX, React-Hooks
Die [FLUX-Architektur](https://facebook.github.io/flux/docs/in-depth-overview.html#content) ist ein Kozept zur Verwaltung des globalen Zustands einer Applikation.

Diese Architektur wird von verschiedenen Bibliotheken implementiert. Die populärste zur Nutzung mit React ist [Redux](https://redux.js.org/).

![flux](https://facebook.github.io/flux/img/flux-simple-f8-diagram-with-client-action-1300w.png)

### 6a: Implementierung mit React-Hooks
Der git-Tag `lesson6a` enthält die Implementierung mit React-eigenen Mitteln. Dazu wurde eine Komponente `StateContainer` implementiert, die den globalen Zustand hält und diesen über einen Context Provider allen Child-Komponenten zur Verfügung stellt. Hierfür wurde der React-Hook `useReducer` verwendet. In den Child-Komponenten wird auf den Zustand und den dispatch-Callback über den React-Hook `useContext` zugegriffen.

Die Anwendung besteht aus einem Eingabebereich mit einer Texteingabe sowie einem Button mit der Aufschrift "+" sowie einem Ausgabebereich mit dem eingebenen Text sowie einem Zähler.

Die Ausgabe-Komponente wurde sowohl als Funktions-basierte Komponente (`Output.jsx`) als auch als Klassen-basierte Komponente (`OutputAsClass.jsx`) implementiert.

### 6b: Implementierung mit REDUX
Der git-Tag `lesson6b` enthält die Implementierung mit Redux.

Installation von redux und react-redux:

```
npm install redux
```

```
npm install react-redux
```

Installation von redux-devtools (optional, aber empfehlenswert zum Debuggen):

```
npm install --save-dev redux-devtools
```

Für redux-devtools wird außerdem eine [Browser-Extension](https://github.com/zalmoxisus/redux-devtools-extension#installation) benötigt.




In `actions.js` wurden zwei Actions definiert:
+ `setText(text)` setzt die Property `text` des Redux `state`-Objekts,
+ `incrementNumber` erhöht die Property `number` des Redux `state`-Objekts um 1.

Die Funktion `reducer` wurde geringfügig angepasst:
+ Die möglichen Werte für die Property `action.type` werden jetzt aus der Datei `actions.js` importiert,
+ Der Rückgabewert bei unbekanntem `action.type` ist der der Funktion übergebene Zustand. Dies ist notwendig, da Redux zur Initialisierung den Reducer mit unbekanntem `action.type` aufruft.

## Lektion 7: Tests mit Jest
[Tests bei Verwendung von Create React App](https://facebook.github.io/create-react-app/docs/running-tests)

Setup:
1. Installation von enzyme und react-test-renderer:

    ```
    npm install --save enzyme enzyme-adapter-react-16 react-test-renderer
    ```

2. Zusätzlich muss die Datei `setupTests.js` angelegt werden (Inhalt siehe Datei).

### Test isolierter Komponenten
+ Jeder Test in Datei mit Suffix test.js
+ Zum Rendern einer Komponente ohne ihre Child-Komponenten kann die Funktion shallow verwendet werden (siehe z.B. `Output.test.js`)

### Test von Redux
+ Siehe `actions.test.js` und `reducer.test.js`.

### Test von Komponenten mit Child-Komponenten sowie Events
+ Siehe `App.test.js`:
    ```
    it('increases counter on click',...
    ```

### Snapshot-Tests
+ Siehe `AppSnapshot.test.js`

## Lektion 8: React-Router
Installation:
```
npm install react-router-dom
```
### 8a: Einfaches Beispiel für React-Router
Das Beispiel besteht aus drei Seiten, die jeweils aus einer Top-Level-Komponente bestehen:
+ `<Home />`
+ `<News />`
+ `<About />`

Der Router arbeitet in der Komponente `<App />`.

### 8b: React-Router mit Bootstrap und Refactoring
Dieses Beispiel besteht aus denselben drei Seiten wie 8a. Die Routen werden nun in einem Array `routes` von Objekten in `App.js` verwaltet. Dies erlaubt eine einfachere Wartung. Dieses Array wird an eine Komponente `<Header />` sowie eine Komponente `<ContentRouter />` übergeben, welche die Titelzeile bzw. den Inhalt je nach Pfad rendern.


## Literatur
### JavaScript inkl. ES6:
+ Philip Ackermann: Professionell entwickeln mit JavaScript, ISBN 978-3-8362-5687-2
+ iX Kompakt Frühjahr 2018, Kapitel "Modernes JavaScript"

    [![ixKompakt](https://shop.heise.de/media/product/2af/ix-kompakt-2018-4018837019054-d9c.png)](https://shop.heise.de/katalog/ix-kompakt-programmieren-heute-1)

### React
+ iX Magazin 02/2019: Manuel Ernst: Seitentest -- React-Anwendungen mit Jest und Enzyme test, S. 48-53

    [![ixMagazin](https://shop.heise.de/media/product/2ac/ix-02-2019-4018837022757-267.jpg)](https://shop.heise.de/katalog/ix-02-2019)

+ web&mobile developer-Magazin 03/2019, Jochen H. Schmidt: Vollwertige Alternative -- React Hooks, S.65-81

    [![we&mobile-Magazin](https://www.webundmobile.de/img/1/0/4/0/2/2/1/titel-03-2019-klein.png)](https://www.webundmobile.de/hefte_data_1679900.html)



---

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
